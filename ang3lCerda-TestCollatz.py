#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Collatz import make_cache, generate_ranges


from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "50 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  50)
        self.assertEqual(j, 100)
    
    def test_read3(self):
        s = "333 888\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  333)
        self.assertEqual(j, 888)

    def test_read4(self):
        s = "11 11\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  11)
        self.assertEqual(j, 11)
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(100, 10 )
        self.assertEqual(v, 119)

    def test_eval_2(self):
        v = collatz_eval(1, 1000 )
        self.assertEqual(v, 179)

    def test_eval_3(self):
        v = collatz_eval(201, 2100 )
        self.assertEqual(v, 182)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, " ")
        self.assertEqual(v, None)
    
    def test_eval_6(self):
        v = collatz_eval(7823, 8824)
        self.assertEqual(v, 252)
        
    def test_eval_7(self):
        v = collatz_eval(23421, 89134 )
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 2, 222, 125)
        self.assertEqual(w.getvalue(), "2 222 125\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 333, 444, 134)
        self.assertEqual(w.getvalue(), "333 444 134\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 50, 150, 122)
        self.assertEqual(w.getvalue(), "50 150 122\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w )
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n900 1000 174\n")
    
    def test_solve1(self):
        r = StringIO("20 40\n60 80\n100 120\n140 160\n")
        w = StringIO()
        collatz_solve(r, w )
        self.assertEqual(
            w.getvalue(), "20 40 112\n60 80 116\n100 120 114\n140 160 117\n")
        
    def test_solve2(self):
        r = StringIO("550 500\n100 1\n201 210\n300 3\n")
        w = StringIO()
        collatz_solve(r, w )
        self.assertEqual(
            w.getvalue(), "550 500 137\n100 1 119\n201 210 89\n300 3 128\n")
        
    def test_solve3(self):
        r = StringIO("78 134\n500000 501000\n222000 222010\n800000 800200\n")
        w = StringIO()
        collatz_solve(r, w )
        self.assertEqual(
            w.getvalue(), "78 134 122\n500000 501000 364\n222000 222010 187\n800000 800200 375\n")

# ----
# main
# ----


if __name__ == "__main__":
    ranges = generate_ranges()
    range_cache = make_cache(ranges)    
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
